DIR=/home/tdido/pacoptiwall
/usr/bin/python $DIR/init_gpio.py
$DIR/pacoptiwall -l1 --led-no-drop-privs --led-cols=64 --led-rows=64 --led-slowdown-gpio=4 --led-pixel-mapper=Rotate:270 \
    $DIR/img/vader.gif \
    $DIR/img/lukevader.gif \
    $DIR/img/yoda.gif \
    $DIR/img/r2bb8.gif \
    $DIR/img/peppa1.gif \
    $DIR/img/peppa2.gif \
    $DIR/img/peppa3.gif \
    $DIR/img/peppa4.gif \
    $DIR/img/poweroff.gif \
    $DIR/snd/vader.wav \
    $DIR/snd/lukevader.wav \
    $DIR/snd/yoda.wav \
    $DIR/snd/r2bb8.wav \
    $DIR/snd/peppa1.wav \
    $DIR/snd/peppa2.wav \
    $DIR/snd/peppa3.wav \
    $DIR/snd/peppa4.wav \
    $DIR/snd/poweroff.wav
